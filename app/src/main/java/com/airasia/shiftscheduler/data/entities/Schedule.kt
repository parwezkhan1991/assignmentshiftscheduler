package com.airasia.shiftscheduler.data.entities

/**
 ******************************************************
 * Project : ShiftScheduler
 ******************************************************
 * Created by Parwez on 09-Dec-2019.
 */
data class Schedule(val scheduleId: String, val day: Int, val shiftEngineers: List<Engineer>)