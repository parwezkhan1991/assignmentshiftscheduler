package com.airasia.shiftscheduler.data.repository

import com.airasia.shiftscheduler.application.services.scheduler.ScheduleService
import com.airasia.shiftscheduler.data.entities.Engineer
import com.airasia.shiftscheduler.data.entities.Schedule

/**
 ******************************************************
 * Project : ShiftScheduler
 ******************************************************
 * Created by Parwez on 09-Dec-2019.
 */
interface ScheduleRepository {
    fun getSchedule(engineerList: ArrayList<Engineer>): List<Schedule>
}


class ScheduleRepositoryImpl : ScheduleRepository {
    override fun getSchedule(engineerList: ArrayList<Engineer>): List<Schedule> {
        val scheduleService = ScheduleService(engineerList)
        return scheduleService.schedules!!
    }

}