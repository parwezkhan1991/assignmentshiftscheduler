package com.airasia.shiftscheduler.data.repository

import com.airasia.shiftscheduler.application.services.ApiService
import com.airasia.shiftscheduler.data.entities.EngineerListResponse
import com.airasia.shiftscheduler.utils.UseCaseResult

/**
 ******************************************************
 * Project : ShiftScheduler
 ******************************************************
 * Created by Parwez on 10-Dec-2019.
 */
interface EngineerListRepository {
    suspend fun getEmployeeList(): UseCaseResult<EngineerListResponse>
}

class EngineerListRepositoryImpl(val apiService: ApiService) : EngineerListRepository {
    override suspend fun getEmployeeList(): UseCaseResult<EngineerListResponse> {
        return try {
            val result = apiService.getEngineersAsync().await()
            UseCaseResult.Success(result)
        } catch (ex: Exception) {
            UseCaseResult.Error(ex)
        }
    }

}