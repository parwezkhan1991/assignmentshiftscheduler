package com.airasia.shiftscheduler.data.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 ******************************************************
 * Project : ShiftScheduler
 ******************************************************
 * Created by Parwez on 10-Dec-2019.
 */
@Parcelize
data class EngineerListResponse(
    var engineers: List<Engineer>
) : Parcelable