package com.airasia.shiftscheduler.data.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 ******************************************************
 * Project : ShiftScheduler
 ******************************************************
 * Created by Parwez on 09-Dec-2019.
 */
@Parcelize
data class Engineer(
    var id: Int?,
    var name: String?
) : Parcelable