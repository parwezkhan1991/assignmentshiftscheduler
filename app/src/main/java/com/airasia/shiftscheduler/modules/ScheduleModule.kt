package com.airasia.shiftscheduler.modules

import com.airasia.shiftscheduler.data.repository.ScheduleRepository
import com.airasia.shiftscheduler.data.repository.ScheduleRepositoryImpl
import com.airasia.shiftscheduler.ui.fragments.schedule.ScheduleViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 ******************************************************
 * Project : ShiftScheduler
 ******************************************************
 * Created by Parwez on 09-Dec-2019.
 */
val scheduleModule = module {
    factory<ScheduleRepository> { ScheduleRepositoryImpl() }
    // Specific viewModel pattern to tell Koin how to build MainViewModel
    viewModel { ScheduleViewModel(scheduleRepository = get()) }
}