package com.airasia.shiftscheduler.modules

import com.airasia.shiftscheduler.data.repository.EngineerListRepository
import com.airasia.shiftscheduler.data.repository.EngineerListRepositoryImpl
import com.airasia.shiftscheduler.ui.fragments.engineer.EngineerListViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 ******************************************************
 * Project : ShiftScheduler
 ******************************************************
 * Created by Parwez on 10-Dec-2019.
 */
val engineerListModule = module {
    factory<EngineerListRepository> { EngineerListRepositoryImpl(apiService = get()) }
    // Specific viewModel pattern to tell Koin how to build MainViewModel
    viewModel { EngineerListViewModel(employeeListRepository = get()) }
}