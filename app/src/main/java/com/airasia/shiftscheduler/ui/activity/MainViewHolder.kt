package com.airasia.shiftscheduler.ui.activity

import androidx.appcompat.widget.Toolbar
import com.google.android.material.appbar.AppBarLayout
import com.airasia.shiftscheduler.R
import com.airasia.shiftscheduler.base.BaseActivity
import io.reactivex.internal.functions.ObjectHelper.requireNonNull


/**
 ******************************************************
 * Project : ShifttScheduler
 ******************************************************
 * Created by Parwez on 11-Dec-2019.
 */
internal class MainViewHolder
/**
 * The constructor purpose is to initialize all the UI components.
 *
 * @param activity the view that will be used to get the UI components.
 */
    (activity: BaseActivity) {

    // The tool bar.
    var toolbar: Toolbar

    // The app bar.
    var appBar: AppBarLayout

    // The base activity.
    private val activity: BaseActivity


    init {
        var activity = activity
        activity = requireNonNull(activity, "activity cannot be null")
        this.activity = activity
        appBar = activity.findViewById(R.id.appBar)
        toolbar = activity.findViewById(R.id.view_toolbar)

    }

}
