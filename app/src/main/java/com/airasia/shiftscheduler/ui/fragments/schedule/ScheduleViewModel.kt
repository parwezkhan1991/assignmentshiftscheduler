package com.airasia.shiftscheduler.ui.fragments.schedule

import androidx.lifecycle.MutableLiveData
import com.airasia.shiftscheduler.base.BaseViewModel
import com.airasia.shiftscheduler.data.entities.Engineer
import com.airasia.shiftscheduler.data.entities.Schedule
import com.airasia.shiftscheduler.data.repository.ScheduleRepository
import kotlinx.coroutines.launch

/**
 ******************************************************
 * Project : ShiftScheduler
 ******************************************************
 * Created by Parwez on 11-Dec-2019.
 */
class ScheduleViewModel(var scheduleRepository: ScheduleRepository) : BaseViewModel() {
    var scheduleList = MutableLiveData<List<Schedule>>()


    fun getSchedule(engineerList: ArrayList<Engineer>) {
        launch {
            scheduleList.value = scheduleRepository.getSchedule(engineerList)
        }
    }
}
