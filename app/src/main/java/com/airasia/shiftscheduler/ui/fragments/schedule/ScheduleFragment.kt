package com.airasia.shiftscheduler.ui.fragments.schedule

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.airasia.shiftscheduler.R
import com.airasia.shiftscheduler.application.Constant
import com.airasia.shiftscheduler.data.entities.Engineer
import com.airasia.shiftscheduler.ui.activity.MainActivity
import kotlinx.android.synthetic.main.schedule_fragment.view.*
import org.koin.android.ext.android.inject

/**
 ******************************************************
 * Project : ShiftScheduler
 ******************************************************
 * Created by Parwez on 11-Dec-2019.
 */
class ScheduleFragment : Fragment() {

    private lateinit var toolbar: Toolbar
    private lateinit var adapter: ScheduleAdapter
    private lateinit var engineerList: ArrayList<Engineer>
    private lateinit var viewParent: View
    private val viewModel: ScheduleViewModel by inject()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null)
            engineerList = arguments!!.getParcelableArrayList<Engineer>(Constant.ENGINEER_LIST)!!
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewParent = inflater.inflate(R.layout.schedule_fragment, container, false)

        return viewParent
    }


    override fun onStart() {
        super.onStart()
        initToolbar()
        initView()
        initViewModel()
        viewModel.getSchedule(engineerList.clone() as ArrayList<Engineer>)
    }

    private fun initView() {
        viewParent.listSchedule.layoutManager =
            LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        adapter = ScheduleAdapter()
        viewParent.listSchedule.adapter = adapter

    }

    private fun initToolbar() {

        val main = activity as MainActivity


        toolbar = main.findViewById(R.id.view_toolbar)

        main.setSupportActionBar(toolbar)
        main.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        main.supportActionBar?.setDisplayShowHomeEnabled(true)
        main.supportActionBar?.title = resources.getString(R.string.schedule)

        toolbar.setNavigationOnClickListener{

            main.onBackPressed()
        }
    }

    private fun initViewModel() {
        viewModel.scheduleList.observe(viewLifecycleOwner, Observer {
            adapter.setData(it)
        })
    }


}
