package com.airasia.shiftscheduler.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toolbar
import androidx.fragment.app.Fragment
import com.airasia.shiftscheduler.R
import com.airasia.shiftscheduler.application.Constant.CUSTOM_INVALID_INT
import com.airasia.shiftscheduler.base.BaseActivity
import com.airasia.shiftscheduler.ui.fragments.engineer.EngineerListFragment
import com.airasia.shiftscheduler.ui.fragments.schedule.ScheduleFragment
import io.reactivex.internal.functions.ObjectHelper
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
    fun showProgress() {
        progressMain.visibility = View.VISIBLE
    }

    fun hideProgress() {
        progressMain.visibility = View.GONE
    }

}
