package com.airasia.shiftscheduler.application.services

import com.airasia.shiftscheduler.data.entities.EngineerListResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET

/**
 ******************************************************
 * Project : ShiftScheduler
 ******************************************************
 * Created by Parwez on 09-Dec-2019.
 */
interface ApiService {

    @GET("engineers")
    fun getEngineersAsync(): Deferred<EngineerListResponse>
}