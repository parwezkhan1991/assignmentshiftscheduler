package com.airasia.shiftscheduler.application.services.scheduler

/**
 ******************************************************
 * Project : ShiftScheduler
 ******************************************************
 * Created by Parwez on 09-Dec-2019.
 */
class ScheduleException(message: String) : RuntimeException(message) {


    companion object {

        // General exception
        const val SCHEDULE_GENERAL_EXCEPTION = "An error occurred while generating the schedule."
        // Invalid exception
        const val SCHEDULE_INVALID_EXCEPTION =
            "Base on the given setting we can not create the schedule."
    }


}