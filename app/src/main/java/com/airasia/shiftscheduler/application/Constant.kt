package com.airasia.shiftscheduler.application

/**
 ******************************************************
 * Project : ShiftScheduler
 ******************************************************
 * Created by Parwez on 09-Dec-2019.
 */
object Constant {

    val CUSTOM_INVALID_INT = -1

    const val BASE_URL = "http://albedotechnology.com/demo_api.php/"

    // Number of working days in schedule period
    const val SCHEDULE_PERIOD_DAYS = 10

    // Engineers required max shifts per day
    const val MAX_SHIFTS_PER_DAY = 2

    // Engineers Should have a day / days off (consecutive)
    const val ENGINEER_OFF_DAYS = 1

    // Engineer shifts
    const val MAX_SHIFTS_PER_ENGINEER = 2

    const val ENGINEER_LIST = "engineer_list"


}