package com.airasia.shiftscheduler.application

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.airasia.shiftscheduler.modules.appModules
import com.airasia.shiftscheduler.modules.engineerListModule
import com.airasia.shiftscheduler.modules.scheduleModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 ******************************************************
 * Project : ShiftScheduler
 ******************************************************
 * Created by Parwez on 09-Dec-2019.
 */
class AppController : Application() {


    override fun onCreate() {
        super.onCreate()
        instance = this
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        startKoin {
            androidContext(this@AppController)
            modules(appModules, engineerListModule, scheduleModule)

        }

    }


    companion object {
        var instance: AppController? = null
    }
}