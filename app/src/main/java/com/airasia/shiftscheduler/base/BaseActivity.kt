package com.airasia.shiftscheduler.base

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.airasia.shiftscheduler.application.Constant.CUSTOM_INVALID_INT
import com.airasia.shiftscheduler.utils.ViewUtils

/**
 ******************************************************
 * Project : ShifttScheduler
 ******************************************************
 * Created by Parwez on 11-Dec-2019.
 */
abstract class BaseActivity : AppCompatActivity() {


    /**
     * Get the attempt content view.
     * To be used by child activities
     *
     * @return layoutResID Resource ID to be inflated.
     */
    @get:LayoutRes
    protected abstract val contentView: Int
    //TODO back button issue
    //TODO product details minimize app issue

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        val contentView = contentView
        // used by activities that don`t need setContentView such as SplashActivity.
        if (contentView != CUSTOM_INVALID_INT) {
            setContentView(contentView)
        }
        if (savedInstanceState != null) {
            onViewReady(savedInstanceState, intent)
        }
    }


    /**
     * This method called after the [AppCompatActivity.setContentView].
     * To be used by child activities
     *
     * @param savedInstanceState A mapping from String keys to various [Parcelable] values.
     * @param intent             intent that started this activity.
     */


    /**
     * Destroy all fragments and loaders.
     */
    override fun onDestroy() {
        super.onDestroy()
    }


    /**
     * Replace the current view to the new one.
     *
     * @param fragment The fragment.
     * @param frameId  The frame id.
     * The `fragment` is added to the container view with id `frameId`. The operation is
     * performed by the `fragmentManager`.
     * @param tag      Optional tag name for the fragment, to later retrieve the
     * @return whether it successfully replaced or not.
     */
    fun replaceFragment(
        fragment: Fragment,
        frameId: Int, @Nullable tag: String, bundle: Bundle?
    ): Boolean {
        val fragmentManager = supportFragmentManager
        ViewUtils.replaceFragment(fragmentManager, fragment, frameId, tag,bundle)
        //TODO add the logic for returning true or false.
        return true
    }

    companion object{

        @CallSuper
        protected fun onViewReady(savedInstanceState: Bundle, intent: Intent) {
            //To be used by child activities
        }
    }


}